Overview: This sample app implements as many Synapse Group technologies as possible within an (ideally) three day timespan.

Technologies:
- Backbone
- Grunt (build to a staging env)
- Testing (Emailed Schell for specifics)
- esLint
 + Google defaults
- BitBucket
- Amazon EC2 (probably skip this one?)